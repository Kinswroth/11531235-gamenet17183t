﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordAnimator : MonoBehaviour 
{

	Animator anim;


	void Start()
	{
		
		anim = GetComponent<Animator>();
		Trigger();
		Destroy(this.gameObject, 0.5f);
	}



	void Trigger()
	{
		anim.SetTrigger("Attack");
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ProjectileBehaviour : NetworkBehaviour 
{
	public float speed = 2;
	public float direction;
	public float damage = 5;



	public virtual void Start()
	{
		Destroy (this.gameObject, 5);
	}

	public virtual void FixedUpdate()
	{
		this.transform.position += new Vector3 ( direction * speed * Time.deltaTime , 0, 0);
	}


	public virtual void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.GetComponent<Health>())
		{
			col.gameObject.GetComponent<Health>().DeductHp(damage);
            col.gameObject.GetComponentInParent<Rigidbody2D>().AddForce(new Vector2(direction * 100.0f, 0), ForceMode2D.Force);
            Destroy (this.gameObject);
		}
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System.Linq;
public class GameManager : NetworkBehaviour 
{
	public List<PlayerController> players;
	public Spawner spawner;
	public static GameManager instance;
	void Awake()
	{
		instance = this;
	}

	public void Start()
	{
		if (IsUIOpen() == false) {
			//SceneManager.LoadSceneAsync ("UI Scene", LoadSceneMode.Additive);
		}
		spawner.Activate ();


	}

	void Update()
	{
		if (players.Any(player => player.GetComponent<Health>().health <= 0))
		{
			SceneManager.LoadSceneAsync("Game Over");
		}
	}

	bool IsUIOpen()
	{
		Scene UIScene = SceneManager.GetSceneByName ("UI Scene");
		for (int i = 0; i < SceneManager.sceneCount; i++) 
		{
			if (SceneManager.GetSceneAt(i) == UIScene) 
			{
				return true;
			}
		}
		return false;
	}




}

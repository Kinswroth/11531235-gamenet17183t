﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class PlayerCombat : NetworkBehaviour
{
    public delegate void AttackEvents();
    public PlayerController actor;
    public LayerMask attackLayers;
    [SyncEvent]
    public event AttackEvents EventOnAttacking;
    [SyncEvent]
    public event AttackEvents EventOnAttacked;
    
    public virtual void Start()
    {
        actor = GetComponent<PlayerController>();
        EventOnAttacking += Attack;
    }

    public virtual void Attack() {  }


    [Command]
    public void CmdInvokeAttack()
    {
        EventOnAttacking.Invoke();
    }
    [Command]
    public void CmdInvokeAttacked()
    {
        EventOnAttacked.Invoke();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class RangedCombat : NetworkBehaviour 
{
	public GameObject projectile;
	public float offset;

	public void Attack (float direction)
	{
		SpawnProjectile (projectile, direction);
	}

	void SpawnProjectile(GameObject proj, float direction)
	{
		if (projectile == null) return;
		GameObject temp = Instantiate (projectile, new Vector2( this.transform.position.x + (offset * direction), transform.position.y), transform.rotation);
		temp.GetComponent<ProjectileBehaviour> ().direction = direction;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MeleeCombat : PlayerCombat {
    public GameObject meleePrefab;
    public Vector3 castSize;
    public float offset = 0.2f;
    public float attackInterval = 0.5f;
    public float knockbackForce = 100f;
    public Collider2D[] colliders;
    IEnumerator IEAttack;
    public override void Attack()
    {
        if(!actor.canCombat) return;
        base.Attack();
        colliders = Physics2D.OverlapBoxAll(new Vector2(this.transform.position.x + (offset * actor.direction.x), transform.position.y), castSize, 0.0f, attackLayers);
        Damager damager = actor.GetComponent<Damager>();
        if(meleePrefab != null)
        {
            GameObject temp = Instantiate(meleePrefab,new Vector2(this.transform.position.x, transform.position.y),meleePrefab.transform.rotation, actor.directions[actor.lastDirection].transform);
        }
        foreach (var item in colliders)
        {
            Health receiver = item.GetComponent<Health>();
            if(receiver == null) continue;
            damager.DealDamage(damager, receiver, damager.damage);
            receiver.gameObject.GetComponentInParent<Rigidbody2D>().AddForce(actor.direction * knockbackForce, ForceMode2D.Force);
        }
        IEAttack = AttackInterval();
        StartCoroutine(IEAttack);
    }


    IEnumerator AttackInterval()
    {
        actor.canMove = false;
        actor.canCombat = false;
        yield return new WaitForSeconds(attackInterval);
        actor.canMove = true;
        actor.canCombat = true;
        CmdInvokeAttacked();
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube( new Vector2(this.transform.position.x + (offset * actor.direction.x), transform.position.y), castSize);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerInput : NetworkBehaviour {
	PlayerController pc;
	// Use this for initialization
	void Start () {
		pc = GetComponent<PlayerController>();
	}
	
	void LateUpdate()
	{
		if (!isLocalPlayer)
			return;
		pc.hit = Physics2D.Raycast(transform.position, -Vector2.up);
		if (pc.canMove == true && pc.ID == 0) {
			if (Input.GetAxisRaw ("Horizontal") < 0 ) {
				pc.CmdInvokeLeft ();
			}
			if (Input.GetAxisRaw ("Horizontal") > 0 ) {
				pc.CmdInvokeRight ();
			}
		
		}
		Camera.main.gameObject.transform.position = new Vector3(transform.position.x, transform.position.y, Camera.main.gameObject.transform.position.z);
		if(Input.GetKeyDown(pc.skill)) 	GetComponent<SkillActor>().CmdUseSkill();
		if(Input.GetKeyDown(pc.switchSkill)) GetComponent<SkillActor>().CmdSkillForward();
		Debug.DrawRay(transform.position, -Vector3.up);
	}

	void Update()
	{
		if (!isLocalPlayer)
			return;
		if (Input.GetKeyDown(KeyCode.Space) ) {
			pc.CmdInvokeJump ();

		}
		if (Input.GetKeyDown(pc.attack) ) {
			GetComponent<PlayerCombat> ().CmdInvokeAttack();

		}
	}
}

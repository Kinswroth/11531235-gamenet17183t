﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlashAnimation : MonoBehaviour
{
    Animator anim;
    public PlayerCombat attack;
    private void Start()
    {
        anim = GetComponent<Animator>();
        attack.EventOnAttacking += Slash;
    }
    public void Slash()
    {
        anim.SetTrigger("Attack");
        anim.ResetTrigger("Attack");
    }
}

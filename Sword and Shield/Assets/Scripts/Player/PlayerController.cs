﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour  
{
	public delegate void ControllerEvents();

	public int ID;
	public float playerSpeed;
	public bool canMove = true;
	public bool canInteract = true;
	public bool canCombat = true;
    public bool canSkill = true;
	public Vector3 direction = new Vector2(1,0);
	public int lastDirection;
	public List<GameObject> directions;
	public Rigidbody2D rb;
	public RaycastHit2D hit;
    public SkillActor skillActor;

	public KeyCode attack = KeyCode.R;
    public KeyCode skill = KeyCode.E;
    public KeyCode switchSkill = KeyCode.Q;

	[Header("Jump Settings")]
	public float floatHeight;
	public float liftForce;

	[SyncEvent]
	public event ControllerEvents EventOnLeft;
	[SyncEvent]
	public event ControllerEvents EventOnRight;
	[SyncEvent]
	public event ControllerEvents EventOnMove;
	[SyncEvent]
	public event ControllerEvents EventOnJump;
    [SyncEvent]
    public event ControllerEvents EventOnSKill;

    public virtual void Start()
	{
		direction = new Vector2(1,0);
        skillActor = GetComponent<SkillActor>();
		rb = GetComponent<Rigidbody2D> ();
		foreach (var item in Input.GetJoystickNames())
		{
			Debug.Log(item);
		}
		EventOnLeft += (MoveLeft);
		EventOnRight +=  (MoveRight);
		EventOnJump += (Jump);
		//EventOnUp.AddListener (MoveUp);
		//EventOnDown.AddListener (MoveDown);
	}


	
	[Command]
	public void CmdInvokeLeft()
	{
		EventOnLeft.Invoke ();
	}
	[Command]
	public void CmdInvokeRight()
	{
		EventOnRight.Invoke ();
	}
	[Command]
	public void CmdInvokeJump()
	{
		EventOnJump.Invoke ();
	}

    public virtual void MoveLeft()
    {
        {
            transform.position += new Vector3(-1 * playerSpeed * Time.deltaTime, 0, 0);
            direction = -transform.right;
            lastDirection = 0;
            //transform.eulerAngles = transform.forward * 90;
            GetComponent<SpriteRenderer>().flipX = false;
        }
    }
    public virtual void MoveRight()
    {
        {
            transform.position += new Vector3(1 * playerSpeed * Time.deltaTime, 0, 0);
            direction = transform.right;
            lastDirection = 1;
            GetComponent<SpriteRenderer>().flipX = true;
            //transform.eulerAngles = -transform.forward * 90;
        }
    }


    public void Jump()
	{
		if (hit.collider != null) 
		{
			if ( (transform.position.y- hit.collider.transform.position.y) <= floatHeight ) 
			{
				rb.AddForce(Vector3.up * liftForce);
			}

		}
	}




}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {
	public Image bar;
	public Health health;

	void Start()
	{
		health.EventOnHealthChange += (UpdateBar);
	}

	public void UpdateBar()
	{
		BarFillAmount (health.health / health.maxHealth);
	}

	public void BarFillAmount(float amt)
	{
		bar.fillAmount = amt;
	}
}

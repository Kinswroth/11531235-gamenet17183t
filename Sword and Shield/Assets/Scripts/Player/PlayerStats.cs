﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerStats : NetworkBehaviour 
{
	public delegate void StatsEvents();
	public float health = 100;
	public float maxHealth = 100;
	public float mana = 100;
	public float maxMana = 100;

	public StatsEvents EventOnStatChange;
	// Use this for initialization
	void Start () 
	{
		EventOnStatChange += ClampMana;
		EventOnStatChange += ClampHealth;
	}
	



	public void AddHealth(float amt)
	{
		health += amt;
		CmdInvokeStatChange ();
	}
	public void DeductHealth(float amt)
	{
		health -= amt;
		CmdInvokeStatChange ();
	}

	public void AddMana(float amt)
	{
		mana += amt;
		CmdInvokeStatChange ();
	}
	public void DeductMana(float amt)
	{
		mana -= amt;
		CmdInvokeStatChange ();
	}

	void ClampHealth()
	{
		if (health < 0)
			health = 0;
		if (health > maxHealth)
			health = maxHealth;
	}

	void ClampMana()
	{
		if (mana < 0)
			mana = 0;
		if (mana > maxMana)
			mana = maxMana;
	}

	[Command]
	public void CmdInvokeStatChange()
	{
		EventOnStatChange.Invoke ();
	}
}

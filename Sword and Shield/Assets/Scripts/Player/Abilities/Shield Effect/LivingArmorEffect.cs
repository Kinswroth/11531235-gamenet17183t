﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivingArmorEffect : MonoBehaviour
{
    public float AddHealth;
    public GameObject OffensiveRole;
    public float BuffDuration;
    public float Timer;
    private void Start()
    {
        OffensiveRole.GetComponent<PlayerStats>().maxHealth += AddHealth;
        OffensiveRole.GetComponent<PlayerStats>().health += AddHealth;
    }
    private void Update()
    {
        Timer += Time.deltaTime;
        if (Timer >= BuffDuration)
        {
            OffensiveRole.GetComponent<PlayerStats>().maxHealth -= AddHealth;
            OffensiveRole.GetComponent<PlayerStats>().health -= AddHealth;
            Destroy(this);
        }
            
        
    }
}

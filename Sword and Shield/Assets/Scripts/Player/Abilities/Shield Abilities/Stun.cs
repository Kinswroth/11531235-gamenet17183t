﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class Stun : Skills
{
    public GameObject StunPrefab;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            CmdUseSkill();
        }
    }
    public override void CmdUseSkill()
    {
        base.CmdUseSkill();
        GameObject Stun = Instantiate(StunPrefab, AbilitySpawn.position, AbilitySpawn.rotation);
        NetworkServer.Spawn(Stun);
    }
}

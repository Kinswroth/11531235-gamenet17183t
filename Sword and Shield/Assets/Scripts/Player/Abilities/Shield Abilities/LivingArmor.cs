﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LivingArmor : Skills {

    public GameObject LivingArmorPrefab;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            CmdUseSkill();
        }
    }
    public override void CmdUseSkill()
    {
        base.CmdUseSkill();
        GameObject LivingArmor = Instantiate(LivingArmorPrefab, AbilitySpawn.position, AbilitySpawn.rotation);
        NetworkServer.Spawn(LivingArmor);
        Destroy(this.gameObject, DespawnTime);
    }
}

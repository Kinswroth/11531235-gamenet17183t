﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SpinSlash : Skills
{
    public GameObject SpinSlashPrefab;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            CmdUseSkill();
        }
    }

    public override void CmdUseSkill()
    {
        base.CmdUseSkill();
        GameObject SpinSlash = Instantiate(SpinSlashPrefab, AbilitySpawn.position, AbilitySpawn.rotation);
        NetworkServer.Spawn(SpinSlash);
        Destroy(this.gameObject, DespawnTime);
    }
}

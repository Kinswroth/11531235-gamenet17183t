﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LifeStab : Skills
{
    public GameObject LifeStabPrefab;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            CmdUseSkill();
        }
    }

    public override void CmdUseSkill()
    {
        base.CmdUseSkill();
        GameObject LifeStab = Instantiate(LifeStabPrefab, AbilitySpawn.position, AbilitySpawn.rotation);
        NetworkServer.Spawn(LifeStab);
        Destroy(this.gameObject, DespawnTime);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinSlashEffect : MeleeBehavior
{
    public void OnCollisionEnter2D(Collision2D col)
    {
        col.gameObject.GetComponent<EnemyStats>().health -= Damage;
        Destroy(this.gameObject);
    }
}

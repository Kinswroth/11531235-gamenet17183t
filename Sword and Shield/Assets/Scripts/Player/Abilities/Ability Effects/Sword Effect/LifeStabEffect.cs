﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeStabEffect : MeleeBehavior
{
    public GameObject DefensiveRole;
    public int HealAmount;
    // Use this for initialization
    public void OnCollisionEnter2D(Collision2D col)
    {
        //heal defensive role
        DefensiveRole.GetComponent<PlayerStats>().health += HealAmount;
        col.gameObject.GetComponent<EnemyStats>().health -= Damage;
        Destroy(this);
    }
}

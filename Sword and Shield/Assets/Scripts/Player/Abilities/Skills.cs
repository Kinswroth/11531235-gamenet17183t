﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class Skills : MonoBehaviour
{
    public int Cooldown;
    public int Damage;
    public int Heal;
    public int DespawnTime;
    public Transform AbilitySpawn;
    //[Command]
    public virtual void CmdUseSkill()
    {

    }
}

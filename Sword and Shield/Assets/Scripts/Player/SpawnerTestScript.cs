﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class SpawnerTestScript : NetworkBehaviour 
{
	public GameObject enemyPrefab;
    public Transform spawnPoint;
    

	void Start () 
	{
        CmdSpawnEnemy();
	}

	void Update () 
	{
		
	}

    [Command]
    void CmdSpawnEnemy()
    {
        GameObject enemyInstance = Instantiate(enemyPrefab, spawnPoint.transform.position, Quaternion.identity);
        NetworkServer.Spawn(enemyInstance);
    }
}

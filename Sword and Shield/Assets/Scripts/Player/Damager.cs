﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Damager : MonoBehaviour 
{
	public float damage;

	public UnityEvent OnDamageDealt;

	public void DealDamage(Damager actor, Health receiver, float damage)
	{
		receiver.DeductHp(actor.damage);
	}
	
}

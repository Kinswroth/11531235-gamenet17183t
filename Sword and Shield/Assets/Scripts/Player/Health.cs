﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
public class Health : NetworkBehaviour {
	public HealthBar bar;
	public delegate void HealthEvents ();
	[SyncVar]
	public float health;
	[SyncVar]
	public float maxHealth;
	[SyncEvent]
	public event HealthEvents EventOnHealthChange;
	[SyncEvent]
	public event HealthEvents EventOnHealthDepleted;


	//public const int maxHealth = 100;
	//public bool isDestroyOnHealth = false;
	//[SyncVar(hook = "OnChangeHealth")]
	//public int currentHealth = maxHealth;

	void Awake()
	{
		health = maxHealth;
	}
		
	public virtual void Start()
	{
		EventOnHealthDepleted += (DestroySelf);
	}



	public virtual void DeductHp(float amt)
	{
		if(!isServer) return;
		if (amt <= 0) amt = 0;

		health -= amt;
		if(health <= 0)
		{
			health = 0;
			CmdInvokeHealthDepleted();	
		} 

		CmdInvokeHealthChange();
	}

	public virtual void DeductHp(float amt, GameObject actor)
	{
		if(!isServer) return;
		if (amt <= 0) amt = 0;

		health -= amt;
		if(health <= 0)
		{
			health = 0;
			CmdInvokeHealthDepleted();	
		} 
		CmdInvokeHealthChange();
	}

	public virtual void DestroySelf()
	{
		Destroy (this.gameObject);
	}

	public virtual void AddHp(float amt)
	{
		if(!isServer) return;
		health += amt;
		if(health > maxHealth) health = maxHealth;
		Debug.Log("heal");
		EventOnHealthChange.Invoke();
	}

	[Command]
	public void CmdInvokeHealthChange()
	{
		EventOnHealthChange.Invoke ();
	}
	[Command]
	public void CmdInvokeHealthDepleted()
	{
		EventOnHealthDepleted.Invoke ();
	}
}

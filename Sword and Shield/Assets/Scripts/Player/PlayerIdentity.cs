﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerIdentity : NetworkBehaviour {
	public CanvasController canvas;
	public GameManager gameManager;

	void Start()
	{
		canvas = CanvasController.instance;
		gameManager = GameManager.instance;
		if (!isLocalPlayer)
			return;
		canvas.localPlayer = this.GetComponent<PlayerController>();
		gameManager.players.Add(this.GetComponent<PlayerController>());
	}



}

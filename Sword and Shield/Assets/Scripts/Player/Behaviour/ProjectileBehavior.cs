﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBehavior : MonoBehaviour {

	public int ProjectileSpeed;
    public float DespawnTime;
    PlayerController Character;
    private void Start()
    {
        Character = GetComponent<PlayerController>();
    }
    void Update ()
    {
        this.GetComponent<Rigidbody2D>().velocity = Character.direction * ProjectileSpeed;

        Destroy(this, DespawnTime);
    }
}

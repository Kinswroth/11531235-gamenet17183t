﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class SkillActor : NetworkBehaviour
{
    public List<BaseSkill> skillsPrefab;
    public List<BaseSkill> skills;
    public BaseSkill currentSkill;
    public int index;
    public UnityEvent EventOnSkillForward;
    public UnityEvent EventOnSkillBackward;

    void Start()
    {
        CmdSpawnSkills();
        currentSkill = skills[0];
        EventOnSkillForward.AddListener(CmdSkillForward);
        EventOnSkillBackward.AddListener(CmdSkillBackward);
    }
    [Command]
    public void CmdSpawnSkills()
    {
          foreach (var item in skillsPrefab)
        {
            BaseSkill temp = Instantiate(item, this.transform);
            skills.Add(temp);
        }
    }
    [Command]
    public void CmdSkillForward()
    {
        if (skills.Count <= 0 || skills.Count == 1) return;
        index++;
        if (index > skills.Count - 1) index = 0;
        currentSkill = skills[index];
    }
    [Command]
    public void CmdSkillBackward()
    {
        if (skills.Count <= 0 || skills.Count == 1) return;
        index--;
        if (index < 0) index = skills.Count - 1;
        currentSkill = skills[index];
    }
    [Command]
    public void CmdUseSkill()
    {
        if(currentSkill.isOnCooldown) return;
        currentSkill.Activate(this);
    }
    
    public void AddSkill(BaseSkill skill)
    {
        skills.Add(skill);
    }

}

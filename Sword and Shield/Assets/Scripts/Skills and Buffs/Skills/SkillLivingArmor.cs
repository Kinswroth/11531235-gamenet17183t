﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillLivingArmor : BaseSkill 
{
	BuffLivingArmor buff;
	public override void Activate(SkillActor actor)
	{
		StartCoroutine(StartCooldown());
		foreach (var player in gameManager.players)
		{
			if (player.ID != actor.GetComponent<PlayerController>().ID)
			{
				player.GetComponent<BuffReceiver>().AddBuff(buff);
			}
		}
	}
	
}

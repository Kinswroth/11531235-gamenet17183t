﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillStun : BaseSkill 
{
	public BuffStun stun;
	public override void Activate(SkillActor actor)
	{
		Collider2D[] colliders;
		MeleeCombat meleeCombat = actor.GetComponent<MeleeCombat>();
		meleeCombat.CmdInvokeAttack();
		colliders = meleeCombat.colliders;
		
		Debug.Log(colliders.Length);

		foreach (var item in colliders)
        {
            BuffReceiver receiver = item.GetComponent<BuffReceiver>();
            Debug.Log("asdasdasdas");
            if(receiver == null) continue;
            receiver.AddBuff(stun);
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SkillLifeStab : BaseSkill 
{
	public float damage;
	public override void Activate(SkillActor actor)
	{
		damage = actor.GetComponent<Damager>().damage;
		PlayerCombat playerCombat = actor.GetComponent<PlayerCombat>();
		
		playerCombat.CmdInvokeAttack();
		foreach (var player in gameManager.players)
		{
			Health receiver = player.GetComponent<Health>(); 
			if (player.ID != actor.GetComponent<PlayerController>().ID)
			{
				receiver.AddHp(damage);
			}
		}
	}
}

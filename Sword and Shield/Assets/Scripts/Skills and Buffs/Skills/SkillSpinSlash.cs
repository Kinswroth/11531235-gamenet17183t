﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Networking;

public class SkillSpinSlash : BaseSkill {
    public BuffSpinSlash buff;
    
    public override void Activate(SkillActor actor)
    {
        StartCoroutine(StartCooldown());
        actor.GetComponent<BuffReceiver>().AddBuff(buff);
    }

}



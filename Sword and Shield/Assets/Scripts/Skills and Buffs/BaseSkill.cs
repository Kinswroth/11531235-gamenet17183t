﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class BaseSkill : NetworkBehaviour
{
    public enum SkillType
    {
        buff,
        attack
    }
    SkillActor actor;
    public string ID;
    public bool isOnCooldown;
    public Sprite sprite;
    public UnityEvent EventOnActivate;
    public float cooldown;

    protected GameManager gameManager;
    public virtual void Start()
    {
        actor = GetComponent<SkillActor>();
        gameManager = GameManager.instance;
        cooldown = 5;
    }

    public IEnumerator StartCooldown()
    {
        isOnCooldown = true;
        yield return new WaitForSeconds(cooldown);
        isOnCooldown = false;
    }

    public virtual void Activate(SkillActor actor) { }
}

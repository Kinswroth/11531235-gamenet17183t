﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffLivingArmor : BaseBuff 
{
	public float healthToAdd = 10;

	void Start()
	{
		EventOnDeactivated.AddListener(ResetHealth);
	}
	public override void Activate(BuffReceiver receiver)
	{
		base.Activate(receiver);
		Health health = receiver.GetComponent<Health>();

		health.maxHealth += healthToAdd;
		health.AddHp (healthToAdd);
	}

	public void ResetHealth(BuffReceiver receiver)
	{
		Health health = receiver.GetComponent<Health>();
		health.maxHealth -= healthToAdd;
		
		if (health.health > healthToAdd)
		{
			health.DeductHp (healthToAdd);
		}
	}
	
}

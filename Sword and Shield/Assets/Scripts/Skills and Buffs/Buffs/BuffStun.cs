﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffStun : BaseBuff 
{
	void Start()
	{
		EventOnDeactivated.AddListener(UnStun);
	}
	public override void Activate(BuffReceiver receiver)
	{
		if(receiver.GetComponent<AIController>())
		{
			AIController ai = receiver.GetComponent<AIController>();
			ai.canMove = false;
		}
	}

	public void UnStun(BuffReceiver receiver)
	{
		if(receiver.GetComponent<AIController>())
		{
			AIController ai = receiver.GetComponent<AIController>();
			ai.canMove = true;
		}
	}
}

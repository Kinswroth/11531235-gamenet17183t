﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffSpinSlash : BaseBuff
{
    public float damage;
    public float interval;
    public float radius;
    public LayerMask layerMask;
	Damager damager;
	IEnumerator IEDamageOverTime;

    Collider2D[] colliders;

	public void Start()
	{
		damager =  receiver.GetComponent<Damager>();
		damage = damager.damage;	
	}

	public override void Activate(BuffReceiver receiver)
	{
		base.Activate(receiver);
		IEDamageOverTime = (DamageOverTime(receiver));
		StartCoroutine(IEDamageOverTime);
		StartCoroutine(DeactivateOnDurationEnd(receiver));
	}

    IEnumerator DamageOverTime(BuffReceiver receiver)
    {
        while (true)
        {
            colliders = Physics2D.OverlapCircleAll(receiver.transform.position, radius, layerMask);
            foreach (var item in colliders)
            {
                Health damageReceiver = item.GetComponent<Health>();
                if (damageReceiver == null || item == null || damager == null) continue;
				damager.DealDamage(damager, damageReceiver, damage);
            }
            yield return new WaitForSeconds(interval);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(receiver.transform.position, radius);
    }
}

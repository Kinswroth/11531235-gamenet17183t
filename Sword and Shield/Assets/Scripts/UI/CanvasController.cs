﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class CanvasController : NetworkBehaviour 
{
	public Image healthBar;
	public Text currentSkill;
	public PlayerController localPlayer;
	public static CanvasController instance;

	void Awake()
	{
		instance = this;
	}

	void Update()
	{
		UpdateUI ();
	}

	void UpdateUI()
	{
		if (localPlayer == null)
			return;
		healthBar.fillAmount = localPlayer.GetComponent<Health> ().health / localPlayer.GetComponent<Health> ().maxHealth;
		currentSkill.text = localPlayer.GetComponent<SkillActor>().currentSkill.ID;
	}

}

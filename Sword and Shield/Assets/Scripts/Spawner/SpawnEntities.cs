﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class SpawnEntities 
{
	public PlayerController prefab;
	public int amountToSpawn;
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class FlyingMonster : AIController
{

    public float diveHeight;
    public float diveStrength;
    public float diveDelayTimer;
    public float knockbackForce;
    public float damage;

    public bool currentlyDiving = false;
    private float PlayerArea = 0.2f;
    private float Timer;
    private Vector2 knockDirection;
   


    private void Start()
    {
        Timer = diveDelayTimer;
        canMove = true;
        rb = this.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (canMove)
        {
            FindTarget();
            if (target == null) return;
                
            CheckTargetDistances();
            //When not diving go to diving point
            if (!currentlyDiving)
            {
                FlyAboveTarget();
            }

            if (CheckIfAbovePlayer())
            {
                //Time delay between each Dive or attack
                if (Timer > 0)
                {
                    Timer--;
                    return;
                }
                else
                {
                    Dive();
                }
            }
            else
            {
                Vector3 dir = target.transform.position - transform.position;
                //Cancels the dive when it hits the area near the player
                if (dir.magnitude <= PlayerArea)
                {
                    currentlyDiving = false;
                }
                //Cancels the dive when the distance between the player and the monster gets too big
                if (CheckIfBelowPlayer() && currentlyDiving == true)
                {
                    currentlyDiving = false;
                }
                Timer = diveDelayTimer;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        //Stops the monster from passing through the bottom floor
        if (col.gameObject.name == "Wall")
        {
            currentlyDiving = false;
        }
        //Damages and knock backs the player
        if (col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<Health>().DeductHp(damage);
            col.gameObject.GetComponentInParent<Rigidbody2D>().AddForce(knockDirection * knockbackForce, ForceMode2D.Force);
        }
    }
    //Check if the monster is below the player
    public bool CheckIfBelowPlayer()
    {
        if (transform.position.y < target.position.y - diveHeight)
        {
            return true;
        }
        else
            return false;
    }
    //Check if the monster is above the player
    public bool CheckIfAbovePlayer()
    {
        if (transform.position.y > target.position.y + diveHeight)
        {
            return true;
        }
        else
            return false;
    }
    //Charges toward the player
    private void Dive()
    {
        Vector3 dir = target.transform.position - transform.position;
        Vector3 dirNorm = dir.normalized;
        rb.velocity = new Vector2((dirNorm.x * diveStrength), (dirNorm.y * diveStrength));
        currentlyDiving = true;
    }
    //Flies above the player  and sets the direction for the knockback
    private void FlyAboveTarget()
    {
        Vector3 dir = target.transform.position - transform.position;
        Vector3 dirNorm = dir.normalized;
        rb.velocity = new Vector2((dirNorm.x * speed), (dirNorm.y * speed) + diveHeight);
        direction = dir.x / Mathf.Abs(dir.x);
        knockDirection = Vector2.right * direction;
    }
}

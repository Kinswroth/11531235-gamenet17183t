﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class DartMonster : AIController
{
    [Header("Dart Monster Variables")]
    public Vector2 fireRate;

    private float nextFire;
    
    protected RangedCombat rangedCombat;

    // Use this for initialization
    void Start()
    {
        nextFire = Random.Range(fireRate.x,fireRate.y);
        canMove = true;
        rb = this.GetComponent<Rigidbody2D>();
        rangedCombat = this.GetComponent<RangedCombat>();
    }

    // Update is called once per frame
    void Update()
    {
        OnTheGround();

        if (canMove)
        {
            if (target == null)
            {
                FindTarget();
            }

            CheckTargetDistances();

            //if the function returns true it will begin chasing
            //if it returns false it will look for the closest jumpoints 
            if (CheckYDiff())
            {
                if (TargetInRange())
                {
                    timer += Time.deltaTime;
                    if (timer > nextFire)
                    {
                        nextFire = Random.Range(fireRate.x,fireRate.y);
                        rangedCombat.Attack(direction);
                        attacking = true;
                        timer = 0;
                    }
                }
                else
                {
                    if (!attacking)
                    {
                        MoveToTarget();
                    }
                }
            }
            else
            {
                if (followPoint == null)
                {
                    LookForFollowPoint();
                }

                MoveToFollowPoint();
            }
        }
    }

    public void MoveToTarget ()
    {
        if (!grounded)
        {
            return;
        }

        if (followPoint != null) followPoint = null;

        Vector3 dir = target.transform.position - transform.position;
        Vector3 dirNorm = dir.normalized;

        direction = dir.x / Mathf.Abs(dir.x);

        rb.velocity = new Vector2(dirNorm.x * (speed), rb.velocity.y);
    }

    //As the title of the function suggests
    public void MoveToFollowPoint()
    {
        if (followPoint != null)
        {
            //For getting the normalized direction for the enemy to follow
            Vector3 dir = followPoint.transform.position - transform.position;
            Vector3 dirNorm = dir.normalized;

            float facing = dir.x / Mathf.Abs(dir.x);
            Vector2 rayDir = Vector2.right * facing;

            RaycastHit2D hit = Physics2D.Raycast(transform.position, rayDir, 0.1f);

            //This is for jumping, it adds velocity along the y to make it jump
            if (hit != false && hit.collider.tag == followPoint.tag)
            {
                if (followPoint.tag == "JumpLeft")
                {
                    rb.velocity = Vector2.zero;
                    rb.velocity = new Vector2(rb.velocity.x + (-jumpForce.x), rb.velocity.y + jumpForce.y);
                    
                    //Vector2 force = new Vector2(-(jumpForce.x), jumpForce.y);
                    //rb.AddForce(force);
                }
                if (followPoint.tag == "JumpRight")
                {
                    rb.velocity = Vector2.zero;
                    rb.velocity = new Vector2(rb.velocity.x + jumpForce.x, rb.velocity.y + jumpForce.y);
                    
                    //Vector2 force = new Vector2(jumpForce.x, jumpForce.y);
                    //rb.AddForce(force);
                }
                followPoint = null;
                return;
            }
            rb.velocity = new Vector2(dirNorm.x * (speed), rb.velocity.y);
        }
    }
}

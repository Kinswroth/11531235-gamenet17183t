﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class PounceMonster : AIController
{
    [Header("Pounce Monster Variables")]
    public Vector2 pounceForce;
    
    public float damage;
    public float attackRate;

    // Use this for initialization
    void Start()
    {
        timer = attackRate;
        canMove = true;
        rb = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        OnTheGround();

        if (canMove)
        {
            if (target == null)
            {
                FindTarget();
            }

            CheckTargetDistances();

            //if the function returns true it will begin chasing
            //if it returns false it will look for the closest jumpoints 
            if (CheckYDiff())
            {
                if (TargetInRange())
                {
                    timer += Time.deltaTime;
                    if (timer > attackRate)
                    {
                        Pounce();
                        timer = 0;
                    }
                    
                    attacking = true;
                }
                else
                {
                    if (!attacking)
                    {
                        MoveToTarget();
                    }
                }
            }
            else
            {
                if (followPoint == null)
                {
                    LookForFollowPoint();
                }

                MoveToFollowPoint();
            }
        }
    }

    public void MoveToTarget()
    {
        if (!grounded)
        {
            return;
        }

        if (followPoint != null) followPoint = null;

        Vector3 dir = target.transform.position - transform.position;
        Vector3 dirNorm = dir.normalized;

        direction = dir.x / Mathf.Abs(dir.x);

        rb.velocity = new Vector2(dirNorm.x * (speed), rb.velocity.y);
    }

    public void MoveToFollowPoint()
    {
        if (followPoint != null)
        {
            Vector3 dir = followPoint.transform.position - transform.position;
            Vector3 dirNorm = dir.normalized;

            direction = dir.x / Mathf.Abs(dir.x);
            Vector2 rayDir = Vector2.right * direction;

            RaycastHit2D hit = Physics2D.Raycast(transform.position, rayDir, 0.1f);

            if (hit != false && hit.collider.tag == followPoint.tag)
            {
                if (followPoint.tag == "JumpLeft")
                {
                    rb.velocity = Vector2.zero;
                    rb.velocity = new Vector2(rb.velocity.x + (-jumpForce.x), rb.velocity.y + jumpForce.y);

                    //Vector2 force = new Vector2(-(jumpForce.x), jumpForce.y);
                    //rb.AddForce(force);
                }
                if (followPoint.tag == "JumpRight")
                {
                    rb.velocity = Vector2.zero;
                    rb.velocity = new Vector2(rb.velocity.x + jumpForce.x, rb.velocity.y + jumpForce.y);

                    //Vector2 force = new Vector2(jumpForce.x, jumpForce.y);
                    //rb.AddForce(force);
                }
                followPoint = null;
                return;
            }
            rb.velocity = new Vector2(dirNorm.x * (speed), rb.velocity.y);
        }
    }

    public void Pounce()
    {
        if (!grounded)
        {
            return;
        }

        rb.velocity = new Vector2(rb.velocity.x + (pounceForce.x * direction), rb.velocity.y + pounceForce.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<Health>() && collision.collider.tag == "Player")
        {
            collision.gameObject.GetComponent<Health>().DeductHp(damage);
            collision.gameObject.GetComponentInParent<Rigidbody2D>().AddForce(new Vector2(direction * 100.0f, 0), ForceMode2D.Force);
        }
    }
}

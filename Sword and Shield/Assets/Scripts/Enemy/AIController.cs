﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using UnityEngine.Networking;

public class AIController : NetworkBehaviour
{
    protected Transform target;
    public float speed;
    public float attackRange;
    public Vector2 jumpForce;

    public LayerMask targetLayer;
    public LayerMask fallLayer;
    public LayerMask jumpLayer;
    public LayerMask wallLayer;

    public bool canMove;
    public float direction;
    public bool attacking;

    protected GameManager gameManager;

    protected bool grounded;
    protected float timer;

    protected Transform followPoint;
    protected Rigidbody2D rb;

    public Vector2 allowedYDiff;
    protected float yDiff;

    //Shoots a raycast that checks if the player is within the target range
    public bool TargetInRange()
    {
        Vector3 dir = target.transform.position - transform.position;
        Vector3 dirNorm = dir.normalized;

        direction = dir.x / Mathf.Abs(dir.x);
        Vector2 rayDir = Vector2.right * direction;

        RaycastHit2D hit = Physics2D.Raycast(transform.position, rayDir, attackRange, targetLayer);

        if (hit != false && hit.collider.tag == "Player")
        {
            return attacking = true;
        }
        return attacking = false;
    }

    //Simple downward raycast that checks if the player is on the ground
    //raycast has a layerMask variable to ignore objects hit that is not of the same type or rather layer
    public bool OnTheGround()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 0.1f, wallLayer);
        if (hit == false)
        {
            return grounded = false;
        }
        return grounded = true;
    }

    //checks if the player is in the allowed Y(Height) value for chasing.
    public bool CheckYDiff()
    {
        if (target != null)
        {
            yDiff = transform.position.y - target.transform.position.y;

            if (yDiff <= allowedYDiff.x && yDiff >= allowedYDiff.y)
            {
                return true;
            }
        }
        return false;
    }

    //Fairly straightforward, its function that gets the player count from
    //the gameManager and then registers any available or existing player as the target.
    public void FindTarget()
    {
        if (gameManager == null) gameManager = GameManager.instance;
        if(gameManager.players.Count == 0) return;
        if (gameManager != null)
        {
            int num = Random.Range(0, gameManager.players.Count);

            if (gameManager.players[num] != null)
                target = gameManager.players[num].transform;
        }
    }

    //Function that checks the distances between active players
    //if another player is nearby this will make it so that it targets the closest player
    public void CheckTargetDistances()
    {
        if (gameManager == null) return;

        if (gameManager.players.Count > 1 && target != null)
        {
            List<float> distances = new List<float>();

            foreach (var item in gameManager.players)
            {
                float dist = Vector2.Distance(transform.position, item.transform.position);

                distances.Add(dist);
            }

            int index = distances.IndexOf(distances.Min());

            target = gameManager.players[index].transform;
        }
    }

    //As the function name suggests, but this shoots a raycast infinitely left and right
    //it then moves registers the closest follow point(Fall or Jump depends on whether the player is above or below)
    public void LookForFollowPoint()
    {
        if (!grounded)
        {
            return;
        }

        if (target != null)
        {
            RaycastHit2D hitRight = Physics2D.Raycast(transform.position, Vector2.up);
            RaycastHit2D hitLeft = Physics2D.Raycast(transform.position, Vector2.up); ;

            //Checks if the player is above of below then proceeds to look for a follow point
            //if the player is below register falllayer as a followpoint and vice versa
            if (transform.position.y > target.transform.position.y)
            {
                hitRight = Physics2D.Raycast(transform.position, Vector2.right, Mathf.Infinity, fallLayer);
                hitLeft = Physics2D.Raycast(transform.position, Vector2.left, Mathf.Infinity, fallLayer);
            }
            if (transform.position.y < target.transform.position.y)
            {
                hitRight = Physics2D.Raycast(transform.position, Vector2.right, Mathf.Infinity, jumpLayer);
                hitLeft = Physics2D.Raycast(transform.position, Vector2.left, Mathf.Infinity, jumpLayer);
            }

            //this checks whether the raycast hit something
            if (hitRight != false && hitLeft == false)
            {
                followPoint = hitRight.collider.transform;
            }
            else if (hitRight == false && hitLeft != false)
            {
                followPoint = hitLeft.collider.transform;
            }
            else if (hitRight != false && hitLeft != false)
            {
                //Checks which is the closer follow point and registers that
                float distRight = Vector2.Distance(hitRight.collider.transform.position, transform.position);
                float distLeft = Vector2.Distance(hitLeft.collider.transform.position, transform.position);

                if (distLeft > distRight)
                {
                    followPoint = hitRight.collider.transform;
                }
                else
                {
                    followPoint = hitLeft.collider.transform;
                }
            }
        }
    }
}

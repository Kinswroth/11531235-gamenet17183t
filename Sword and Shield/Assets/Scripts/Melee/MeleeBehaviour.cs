﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeBehaviour : MonoBehaviour
{
    public float damage;
    public Vector3 direction;
    public float knockbackForce = 100f;

    private void Start()
    {
        Destroy(this.gameObject, 0.08f);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Health health = collision.gameObject.GetComponent<Health>();
        if (health.CompareTag("Player")) return;
        if (health)
        {
            health.DeductHp(damage);
            health.gameObject.GetComponentInParent<Rigidbody2D>().AddForce(direction * knockbackForce, ForceMode2D.Force);
        }
    }


}
